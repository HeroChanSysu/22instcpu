`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/04/28 10:54:58
// Design Name:
// Module Name: alu_tb
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module alu_tb();

    reg[31:0] dataA,dataB;
    reg[3:0] aluctr;
    wire[31:0] dataout;
    wire zero;
    wire gtz;

    alu alu_u0(dataA,dataB,aluctr,dataout,zero,gtz);

    initial begin
        dataA = 32'd23;
        dataB = 32'd23;
        aluctr = 0;

        forever begin
            #20 aluctr = aluctr + 4'b1;
        end
    end

endmodule
