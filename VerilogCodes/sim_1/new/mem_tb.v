`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/05/02 12:25:09
// Design Name:
// Module Name: mem_tb
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module mem_tb();

    reg clk;
    initial clk = 0;
    always #10 clk = ~clk;

    reg[31:0] address,data_wr;
    reg dmen,dmwe;
    reg bysel,bycopy,byexpand;
    wire[31:0] data_rd1,data_rd2;

    inst_rom inst_rom_u1(clk,address,data_rd1);
    data_ram data_ram_u1(address,data_wr,clk,dmen,dmwe,bysel,bycopy,byexpand,data_rd2);

    initial begin
        dmwe = 0;
        dmen = 0;
        bysel = 0;
        bycopy = 0;
        byexpand = 0;

        data_wr = 32'H0000_0000;
        address = 32'H0040_0000;
        repeat(10) begin
            @(posedge clk);
            #2 address = address + 32'H4;
        end
        @(posedge clk);
        @(posedge clk);
        #5;
        dmen = 1;dmwe = 1;
        // data_wr = 32'H0000_0000;
        data_wr = 32'H00000088;
        address = 32'H1001_0000;
        repeat(10) begin
            @(posedge clk);
            #5;
            address = address + 32'H4;
            // data_wr = data_wr + 32'H1;
        end
        @(posedge clk);
        #5;
        dmen = 0;dmwe = 0;

        #2000;
        dmen = 1;dmwe = 0;
        data_wr = 32'H0000_0000;
        address = 32'H1001_0000;
        repeat(20) begin
            @(posedge clk);
            #5;
            address = address + 32'H1;
            data_wr = 32'H88;
            dmwe = 0;
            bysel = 0;
            bycopy = 0;
            byexpand = 0;

            @(posedge clk);
            #5;
            dmwe = 1;
            bysel = 1;
            bycopy = 1;
            byexpand = 0;

            @(posedge clk);
            #5;
            dmwe = 0;
            bysel = 0;
            bycopy = 0;
            byexpand = 0;

            @(posedge clk);
            #5;
            dmwe = 0;
            bysel = 1;
            bycopy = 0;
            byexpand = 1;
        end


    end


endmodule
