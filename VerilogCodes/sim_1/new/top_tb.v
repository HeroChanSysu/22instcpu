`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/05/02 12:58:43
// Design Name:
// Module Name: top_tb
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module top_tb();

    reg clk;
    reg rst;
    initial clk = 0;
    always #10 clk = ~clk;

    initial begin
        rst = 1;
        #35;
        rst = 0;
    end

    reg[3:0] key_pins;
    wire[3:0] led_pins;
    wire[5:0] seg_select_pins;
    wire[7:0] seg_code_pins;
    wire tx_pin;
    wire rx_pin;

    cpu_u cpu_u1(
        .rst_n(~rst),
        .sys_clk(clk),
        .key_pins(key_pins),
        .led_pins(led_pins),
        .seg_select_pins(seg_select_pins),
        .seg_code_pins(seg_code_pins),
        .tx_pin(tx_pin),
        .rx_pin(rx_pin)
    );

    integer key_idx;
    reg[31:0] tx_data;
    reg tx_enable;
    wire tx_ready;
    reg[31:0] codes[41:0];
    integer send_cnt;
    wire perip_clk;
    assign perip_clk = cpu_u1.clks[3];

    initial begin
        key_idx = 0;
        key_pins = 4'b1111;
        send_cnt = 0;
        tx_enable = 0;
        tx_data = 0;
        $readmemh("testprogram.txt",codes,0,41);

        @(negedge rst);
        #3000;
        repeat(42) begin
            @(negedge perip_clk);
            tx_data = codes[send_cnt];
            tx_enable = 1;
            @(negedge perip_clk); tx_enable = 0;
            @(posedge tx_ready);
            #100;
            send_cnt = send_cnt + 1;
        end
        #5000;
        key_pins = 4'b1110;
        #50000000;
        key_pins = 4'b1111;

        // forever begin
        //     #1000000;
        //     key_pins = 4'b1111;
        //     key_pins[key_idx] = 0;
        //     #50000000;
        //     key_pins = 4'b1111;
        //     key_idx = key_idx + 1;
        //     if(key_idx >3)
        //         key_idx = 0;
        // end


    end

    uart_tx_word uart_tx_word_u1(
        .clk(perip_clk),
        .rst_n(~rst),
        .tx_data(tx_data),
        .tx_data_valid(tx_enable),
        .tx_data_ready(tx_ready),
        .tx_pin(rx_pin)
    );



endmodule
