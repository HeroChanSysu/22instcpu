`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/04/28 11:29:10
// Design Name:
// Module Name: rf_tb
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module rf_tb();

    reg[31:0] instW,aluD,memD;
    reg rfwe,reset;
    reg[1:0] asel,dsel,alusel;
    reg[31:0] pc;
    wire[31:0] aluA,aluB;

    reg[4:0] rs,rt,rd;
    reg clock;

    rf_u rf_u0(instW,aluD,memD,rfwe,asel,dsel,alusel,pc,reset,clock,aluA,aluB);

    always @ ( * ) begin
        instW = {6'b000000,rs,rt,rd,5'b0,6'b100001};
    end

    initial begin
        clock = 0;
        rfwe = 0;
        asel = 2;
        dsel = 2;
        alusel = 3;
        reset = 0;
        rs = 1;
        rt = 2;
        rd = 3;
        aluD = 123;
        memD = 312;
        pc = 1145140;
        #5 reset = 1;

        #55 reset = 0;

        repeat(36) begin
            @(negedge clock);
            asel = asel + 1;
            if(asel > 2) begin
                asel = 0;
                dsel = dsel + 1;
                if(dsel > 2) begin
                    dsel = 0;
                    alusel = alusel + 1;
                end
            end

            rfwe = 1;
            @(negedge clock);
            rfwe = 0;
        end


    end

    always #25 clock = ~clock;

endmodule
