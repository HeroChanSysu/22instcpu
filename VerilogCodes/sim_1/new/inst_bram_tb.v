`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/06/02 11:07:19
// Design Name:
// Module Name: inst_bram_tb
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module inst_bram_tb();

    reg clk;
    reg rst;
    initial clk = 0;
    always #10 clk = ~clk;


    reg[31:0] addr;
    wire[31:0] data;

    initial begin
        rst = 1;

        #202;
        rst = 0;

        @(negedge clk);
        addr = 32'H8FC0_0000;
        repeat(4) begin
            @(negedge clk);
            addr = addr + 32'd4;
        end

        @(negedge clk);
        addr = 32'H0040_0000;
        repeat(128) begin
            @(negedge clk);
            addr = addr + 32'd4;
        end

    end

    inst_rom inst_rom_u1(
        .clk(clk),
        .addr(addr),
        .data(data)
    );
endmodule
