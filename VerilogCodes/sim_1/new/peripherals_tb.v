`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/06/04 15:21:54
// Design Name:
// Module Name: peripherals_tb
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module peripherals_tb();

    reg clk;
    reg rst;
    initial clk = 0;
    always #50 clk = ~clk;

    reg[31:0] addr;
    reg[31:0] data_wr;
    wire[31:0] data_rd;
    reg[3:0] key_pins;
    reg en,we;

    localparam  PERIP_KEYS_ADDR = 32'H1002_0000;
    localparam  PERIP_LEDS_ADDR = 32'H1002_0004;
    localparam  PERIP_SEG_CONFIG_ADDR = 32'H1002_0008;
    localparam  PERIP_SEG_VAL_ADDR = 32'H1002_000C;

    initial begin
        rst = 1;
        en = 0;
        we = 0;
        addr = 0;
        data_wr = 0;

        #202;
        rst = 0;
        en = 1;
        #200;

        @(negedge clk);we = 1;
        addr = PERIP_LEDS_ADDR;
        data_wr=  32'H0000000f;
        @(negedge clk); we = 0;

        @(negedge clk);we = 1;
        addr = PERIP_LEDS_ADDR;
        data_wr=  32'H00000000;
        @(negedge clk); we = 0;

        @(negedge clk);we = 1;
        addr = PERIP_SEG_CONFIG_ADDR;
        data_wr=  32'H0000_00_ff;
        @(negedge clk); we = 0;

        @(negedge clk);we = 1;
        addr = PERIP_SEG_VAL_ADDR;
        data_wr=  32'H00ffffff;
        @(negedge clk); we = 0;

        #2000;
        @(negedge clk);we = 1;
        addr = PERIP_SEG_VAL_ADDR;
        data_wr=  32'H00123456;
        @(negedge clk); we = 0;

        #2000;
        @(negedge clk);we = 0;
        addr = PERIP_KEYS_ADDR;
    end

    integer key_idx;

    initial begin
        key_idx = 0;
        key_pins = 4'b1111;
        @(negedge rst);
        forever begin
            #200000;
            key_pins = 4'b1111;
            key_pins[key_idx] = 0;
            #200000000;
            key_pins = 4'b1111;
            key_idx = key_idx + 1;
            if(key_idx >3)
                key_idx = 0;
        end
    end

    peripherals peripherals(addr,data_wr,clk,rst,en,we,data_rd,key_pins);


endmodule
