`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/05/01 19:23:53
// Design Name:
// Module Name: ctrl_u_tb
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module ctrl_u_tb();

    reg[31:0] instW;
    reg fbzero;
    reg fbgtz;
    wire[1:0] pcsrc;
    wire[3:0] aluctr;
    wire rfwe, dmen, dmwe;
    wire[1:0] asel, dsel, alusel;
    wire bysel,bycopy,byexpand;
    reg[5:0] op, func;

    ctrl_u ctrl_u0 (instW, fbzero,fbgtz, pcsrc, aluctr, rfwe, asel, dsel, alusel, dmen, dmwe, bysel,bycopy,byexpand);

    always@ (*)
        instW = {op, 5'b00001, 5'b00010, 5'b00011, 5'b0, func};

    initial begin
        fbzero = 1 ;
        fbgtz = 1;
        #10 op = 6'b000_000; func = 6'b100_001; // 1 addu
        #20 op = 6'b001_001; func = 6'b000_000; // 2 addiu
        #20 op = 6'b001_101; func = 6'b000_000;//3ori.
        #20 op = 6'b000_000; func = 6'b100_110; // 4 xor
        #20 op = 6'b001_111; func = 6'b000_000; // 5 lui
        #20 op = 6'b000_101; func = 6'b000_000; fbzero = 0; // 6 bne, equal
        #20 op = 6'b000_101; func = 6'b000_000; fbzero = 1; // 6 bne, not equal
        #20 op = 6'b000_000; func = 6'b001_000; // 7 jr
        #20 op = 6'b100_011; func = 6'b000_000; // 8 1w
        #20 op = 6'b101_011; func = 6'b000_000; // 9 SW

        #20 op = 6'b011_100; func = 6'b000_010; // 10 MUL
        #20 op = 6'b000_000; func = 6'b100_100; // 11 AND
        #20 op = 6'b001_100; func = 6'b000_000; // 12 ANDI
        #20 op = 6'b000_000; func = 6'b100_101; // 13 OR
        #20 op = 6'b001_110; func = 6'b000_000; // 14 XORI
        #20 op = 6'b000_000; func = 6'b000_000; // 15 SLL
        #20 op = 6'b000_000; func = 6'b000_010; // 16 SRL
        #20 op = 6'b000_100; func = 6'b000_000; fbzero = 0;  // 17 BEQ
        #20 op = 6'b000_100; func = 6'b000_000; fbzero = 1;  // 17 BEQ
        #20 op = 6'b000_111; func = 6'b000_000; fbgtz = 0; // 18 BGTZ
        #20 op = 6'b000_111; func = 6'b000_000; fbgtz = 1;  // 18 BGTZ
        #20 op = 6'b000_010; func = 6'b000_000; // 19 J
        #20 op = 6'b000_011; func = 6'b000_000; // 20 JAL
        #20 op = 6'b100_000; func = 6'b000_000; // 21 LB
        #20 op = 6'b101_000; func = 6'b000_000; // 12 SB

        #20 op = 6'b000_000; func = 6'b100_001; // 1 addu
    end


endmodule
