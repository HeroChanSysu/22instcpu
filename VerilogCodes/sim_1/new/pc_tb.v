`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/04/28 11:05:36
// Design Name:
// Module Name: pc_tb
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module pc_tb();

    reg[31:0] datars,dataoffset;
    reg[25:0] datatarget;
    reg[1:0] pcsrc;
    reg clock,reset;
    wire[31:0] pcout;

    pc_u pc_u1(datars,dataoffset,datatarget,pcsrc,clock,reset,pcout);

    initial clock = 0;
    always #10 clock = ~clock;

    initial begin
        reset = 1;
        dataoffset = 23;
        datatarget = 13;
        datars = 18;
        pcsrc = 0;
        #25;
        reset = 0;

        forever begin
            @(negedge clock);
            pcsrc = pcsrc + 2'b1;
        end
    end
endmodule
