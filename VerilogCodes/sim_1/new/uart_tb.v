`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/06/08 16:01:26
// Design Name:
// Module Name: uart_tb
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module uart_tb();

    reg clk;
    reg rst;
    initial clk = 0;
    always #10 clk = ~clk;

    reg tx_data_valid;
    reg[31:0] tx_data;
    wire rx_data_valid;
    wire[31:0] rx_data;

    wire tx_pin;
    wire tx_ready;


    initial begin
        rst = 1;
        tx_data_valid = 0;
        tx_data = 32'H12345678;
        #42;
        rst = 0;

        #200;
        @(posedge clk);
        #1 tx_data_valid = 1;



    end

    uart_tx_word uart_tx_word_u1(
        .clk(clk),
        .rst_n(~rst),
        .tx_data(tx_data),
        .tx_data_valid(tx_data_valid),
        .tx_data_ready(tx_ready),
        .tx_pin(tx_pin)
    );

    uart_rx_word uart_rx_word_u1(
        .clk(clk),
        .rst_n(~rst),
        .rx_data(rx_data),
        .rx_data_valid(rx_data_valid),
        .rx_pin(tx_pin)
    );

endmodule
