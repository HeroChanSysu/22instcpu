`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/04/28 10:47:24
// Design Name:
// Module Name: alu
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module alu(
    input[31:0] dinA,dinB,
    input[3:0] aluctr,
    output[31:0] dout,
    output zero,
    output gtz
);

    reg[31:0] result;
    assign dout = result;
    assign zero = (aluctr == 4'b0101)&(~(|result));
    assign gtz = (aluctr == 4'b1010)&(~result[31])&(|result);

    wire signed[31:0] dinA_sign;
    wire signed[31:0] dinB_sign;
    wire signed[31:0] a_mul_b_sign;
    assign dinA_sign = dinA;
    assign dinB_sign = dinB;
    assign a_mul_b_sign = dinA_sign*dinB_sign;

    always @ ( * ) begin
        case(aluctr)
            4'b0000: result = 0;
            4'b0001: result = dinA + dinB;
            4'b0010: result = dinA | dinB;
            4'b0011: result = dinA ^ dinB;
            4'b0100: result = {dinB,{16{1'b0}}};
            4'b0101: result = dinA - dinB;
            4'b0110: result = a_mul_b_sign;
            4'b0111: result = dinA & dinB;
            4'b1000: result = dinB << dinA;
            4'b1001: result = dinB >> dinA;
            4'b1010: result = dinA - dinB;
            default: result = 0;
        endcase
    end

endmodule
