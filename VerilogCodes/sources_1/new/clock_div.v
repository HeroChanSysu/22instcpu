`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/06/02 21:56:28
// Design Name:
// Module Name: clock_div
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module clock_div(
    input rst,
    input clk_in,
    output[4:0] clk_out
    );

    reg[4:0] clk_out;
    reg state;
    localparam  S_WAIT = 0;
    localparam  S_OUT = 1;
    reg[7:0] wait_cnt;

    always@(posedge clk_in or posedge rst) begin
        if(rst) begin
            clk_out <= 5'b00000;
            state <= S_WAIT;
            wait_cnt <= 8'd0;
        end
        else if(state == S_WAIT) begin
            if(wait_cnt == 8'd19) begin
                wait_cnt <= 8'd0;
                state <= S_OUT;
                clk_out <= 5'b00001;
            end
            else begin
                wait_cnt <= wait_cnt + 1'b1;
            end
        end
        else if(state == S_OUT) begin
            clk_out <= {clk_out[3:0],clk_out[4]};
        end
    end

endmodule
