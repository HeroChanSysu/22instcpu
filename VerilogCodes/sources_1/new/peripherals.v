`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/06/03 12:53:43
// Design Name:
// Module Name: peripherals
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module peripherals(
    input[31:0] addr,datain,
    input clk,
    input rst,
    input en,we,
    output reg[31:0] dataout,

    input[3:0] key_pins,
    output[3:0] led_pins,
    output[5:0] seg_select_pins,
    output[7:0] seg_code_pins,
    input rx_pin,
    output tx_pin
    );

    wire[3:0] keys;
    reg[3:0] leds;
    reg[23:0] seg_val;
    reg[5:0] seg_dots;
    reg[5:0] seg_ons;

    reg rx_received;
    reg[31:0] rx_data_latch;
    wire rx_valid;
    wire[31:0] rx_data;

    reg tx_enable; // when tx finish, tx_enable reset
    wire tx_ready;
    reg tx_ready_r;
    wire tx_ready_posedge;
    assign tx_ready_posedge = (~tx_ready_r) & tx_ready;

    reg[31:0] tx_data;

    assign led_pins = ~leds;

    localparam  PERIP_KEYS_ADDR = 32'H1002_0000;
    localparam  PERIP_LEDS_ADDR = 32'H1002_0004;
    localparam  PERIP_SEG_CONFIG_ADDR = 32'H1002_0008;
    localparam  PERIP_SEG_VAL_ADDR = 32'H1002_000C;
    localparam  PERIP_UART_TX_CONFIG_ADDR = 32'H1002_0010;
    localparam  PERIP_UART_TX_DATA_ADDR = 32'H1002_0014;
    localparam  PERIP_UART_RX_CONFIG_ADDR = 32'H1002_0018;
    localparam  PERIP_UART_RX_DATA_ADDR = 32'H1002_001C;

    always@(posedge clk or posedge rst) begin
        if(rst) begin
            dataout <= 32'b0;
        end
        else if(en && ~we) begin
            case(addr)
                PERIP_KEYS_ADDR:
                    dataout <= {28'b0,~keys};
                PERIP_LEDS_ADDR:
                    dataout <= {28'b0,leds};
                PERIP_SEG_CONFIG_ADDR:
                    dataout <= {16'b0,2'b0,seg_dots,2'b0,seg_ons};
                PERIP_SEG_VAL_ADDR:
                    dataout <= {8'b0,seg_val};
                PERIP_UART_TX_CONFIG_ADDR:
                    dataout <= {31'b0,tx_enable};
                PERIP_UART_TX_DATA_ADDR:
                    dataout <= {tx_data};
                PERIP_UART_RX_CONFIG_ADDR:
                    dataout <= {31'b0,rx_received};
                PERIP_UART_RX_DATA_ADDR:
                    dataout <= {rx_data_latch};
                default:
                    dataout <= 32'b0;
            endcase
        end
        else begin
            dataout <= 32'b0;
        end
    end

    always@(posedge clk or posedge rst) begin
        if(rst) begin
            leds <= 4'b0;
            seg_val <= 24'b0;
            seg_dots <= 6'b0;
            seg_ons <= 6'b0;
            tx_enable <= 1'b0;
            rx_received <= 1'b0;
            tx_data <= 32'b0;
        end
        else begin
            if(addr == PERIP_UART_TX_CONFIG_ADDR && en && we) begin
                tx_enable <= datain[0];
            end
            else if(tx_ready_posedge) begin
                tx_enable <= 1'b0;
            end
            if(addr == PERIP_UART_RX_CONFIG_ADDR && en && we) begin
                rx_received <= datain[0];
            end
            else begin
                if(rx_valid)
                    rx_received <= 1'b1;
            end

            if(en && we) begin
                if(addr == PERIP_LEDS_ADDR) begin
                    leds <= datain[3:0];
                end
                if(addr == PERIP_SEG_CONFIG_ADDR) begin
                    seg_dots <= datain[13:8];
                    seg_ons <= datain[5:0];
                end
                if(addr == PERIP_SEG_VAL_ADDR) begin
                    seg_val <= datain[23:0];
                end
                if(addr == PERIP_UART_TX_DATA_ADDR) begin
                    tx_data <= datain;
                end
            end
        end
    end

    genvar i;
    generate
        for(i=0;i<4;i=i+1) begin
            ax_debounce ax_debounce_u(
                .clk(clk),
                .rst(rst),
                .button_in(key_pins[i]),
                .button_out(keys[i])
            );
        end
    endgenerate

    SegDisplay SegDisplay(
        .rstn(~rst),
        .clk(clk),
        .numbers(seg_val),
        .dots(seg_dots),
        .ons(seg_ons),
        .select(seg_select_pins),
        .segcode(seg_code_pins)
    );

    uart_tx_word uart_tx_word_u1(
        .clk(clk),
        .rst_n(~rst),
        .tx_data(tx_data),
        .tx_data_valid(tx_enable),
        .tx_data_ready(tx_ready),
        .tx_pin(tx_pin)
    );

    uart_rx_word uart_rx_word_u1(
        .clk(clk),
        .rst_n(~rst),
        .rx_data(rx_data),
        .rx_data_valid(rx_valid),
        .rx_pin(rx_pin)
    );

    always@(posedge clk or posedge rst) begin
        if(rst) begin
            tx_ready_r <= 1'b0;
            rx_data_latch <= 32'b0;
        end
        else begin
            tx_ready_r <= tx_ready;
            if(rx_valid) begin
                rx_data_latch <= rx_data;
            end
        end
    end
endmodule
