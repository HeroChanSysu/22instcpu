`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/06/08 15:43:24
// Design Name:
// Module Name: uart_tx_word
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module uart_tx_word(
    input                        clk,              //clock input
	input                        rst_n,            //asynchronous reset input, low active
	input[31:0]                  tx_data,          //data to send
	input                        tx_data_valid,    //data to be sent is valid
	output reg                   tx_data_ready,    //send ready
	output                       tx_pin            //serial data output
    );

    reg[31:0] tx_data_latch;

    reg tx_data_valid_r;
    wire tx_en;
    assign tx_en = (~tx_data_valid_r) && tx_data_valid; // posedge

    always@(posedge clk or negedge rst_n) begin
        if(~rst_n) begin
            tx_data_valid_r <= 1'b0;
        end
        else begin
            tx_data_valid_r <= tx_data_valid;
        end
    end

    reg[7:0] tx_byte;
    reg tx_byte_valid;
    wire tx_byte_ready;

    reg[2:0] tx_state;
    reg[2:0] tx_state_r;
    localparam  S_TX_BYTE0 = 0;
    localparam  S_TX_BYTE1 = 1;
    localparam  S_TX_BYTE2 = 2;
    localparam  S_TX_BYTE3 = 3;
    localparam  S_TX_OVER = 4;

    always@(posedge clk or negedge rst_n) begin
        if(~rst_n) begin
            tx_state_r <= S_TX_BYTE0;
        end
        else begin
            tx_state_r <= tx_state;
        end
    end

    always@(posedge clk or negedge rst_n) begin
        if(~rst_n) begin
            tx_state <= S_TX_BYTE0;
            tx_data_latch <= 32'b0;
            tx_data_ready <= 1'b0;
            tx_byte_valid <= 1'b0;
            tx_byte <= 8'b0;
        end
        else if(tx_state == S_TX_BYTE0) begin
            if(tx_en) begin
                tx_data_latch <= tx_data;
                // tx_byte <= tx_data[31:24];
                tx_byte <= tx_data[7:0];
                tx_byte_valid <= 1'b1;
                tx_state <= S_TX_BYTE1;
                tx_data_ready <= 1'b0;
            end
            else begin
                tx_data_ready <= 1'b1;
            end
        end
        else if(tx_state == S_TX_BYTE1) begin
            if(tx_state_r == S_TX_BYTE0) begin
                tx_byte_valid <= 1'b0;
            end
            else if(tx_byte_ready) begin
                // tx_byte <= tx_data_latch[23:16];
                tx_byte <= tx_data_latch[15:8];
                tx_byte_valid <= 1'b1;
                tx_state <= S_TX_BYTE2;
            end
        end
        else if(tx_state == S_TX_BYTE2) begin
            if(tx_state_r == S_TX_BYTE1) begin
                tx_byte_valid <= 1'b0;
            end
            else if(tx_byte_ready) begin
                // tx_byte <= tx_data_latch[15:8];
                tx_byte <= tx_data_latch[23:16];
                tx_byte_valid <= 1'b1;
                tx_state <= S_TX_BYTE3;
            end
        end
        else if(tx_state == S_TX_BYTE3) begin
            if(tx_state_r == S_TX_BYTE2) begin
                tx_byte_valid <= 1'b0;
            end
            else if(tx_byte_ready) begin
                // tx_byte <= tx_data_latch[7:0];
                tx_byte <= tx_data[31:24];
                tx_byte_valid <= 1'b1;
                tx_state <= S_TX_OVER;
            end
        end
        else if(tx_state == S_TX_OVER) begin
            if(tx_state_r == S_TX_BYTE3) begin
                tx_byte_valid <= 1'b0;
            end
            else if(tx_byte_ready) begin
                tx_state <= S_TX_BYTE0;
                tx_data_ready <= 1'b1;
            end
        end
    end

    uart_tx #(.CLK_FRE(10)) uart_tx_u1(
        .clk(clk),
        .rst_n(rst_n),
        .tx_data(tx_byte),
        .tx_data_valid(tx_byte_valid),
        .tx_data_ready(tx_byte_ready),
        .tx_pin(tx_pin)
    );

endmodule
