`timescale 1ns / 100ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2021/06/15 20:12:56
// Design Name:
// Module Name: SegDisplay
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module SegDisplay(
    input rstn,
    input clk,
    input numbers,
    input dots,
    input ons,
    output select,
    output segcode
    );
    wire[23:0] numbers;
    wire[5:0] dots;
    wire[5:0] ons;
    wire[5:0] select;
    wire[7:0] segcode;

    reg[5:0] select_reg;
    assign select=select_reg;
    reg[3:0] digit;
    reg on;
    reg dot;

    wire ms_overflow;
    counter #(.MAX_CNT(9999)) counter_u1(
        .rstn(rstn),
        .clk(clk),
        .overflow(ms_overflow)
    );

    reg [2:0] state;

    always@(negedge rstn or posedge clk) begin
        if(!rstn) begin
            state <= 3'b000;
        end
        else if(ms_overflow && state==3'b101) begin
            state <= 3'b000;
        end
        else if(ms_overflow) begin
            state <= state + 1;
        end
    end

    always@(negedge rstn or posedge clk) begin
        if(!rstn) begin
            select_reg <= 6'b11_1111;
            digit <= 0;
            on <= 0;
            dot <= 0;
        end
        else begin
            case(state)
                3'b000: begin
                    select_reg <= 6'b11_1110;
                    digit <= numbers[3:0];
                    on <= ons[0];
                    dot <= dots[0];
                end
                3'b001: begin
                    select_reg <= 6'b11_1101;
                    digit <= numbers[7:4];
                    on <= ons[1];
                    dot <= dots[1];
                end
                3'b010: begin
                    select_reg <= 6'b11_1011;
                    digit <= numbers[11:8];
                    on <= ons[2];
                    dot <= dots[2];
                end
                3'b011: begin
                    select_reg <= 6'b11_0111;
                    digit <= numbers[15:12];
                    on <= ons[3];
                    dot <= dots[3];
                end
                3'b100: begin
                    select_reg <= 6'b10_1111;
                    digit <= numbers[19:16];
                    on <= ons[4];
                    dot <= dots[4];
                end
                3'b101: begin
                    select_reg <= 6'b01_1111;
                    digit <= numbers[23:20];
                    on <= ons[5];
                    dot <= dots[5];
                end
                default:begin
                    select_reg <= 6'b11_1111;
                    digit <= 0;
                    on <= 0;
                    dot <= 0;
                end
            endcase
        end
    end

    SegEncode segencode_u1(
        .number(digit),
        .on(on),
        .dot(dot),
        .seg_code(segcode)
    );

endmodule
