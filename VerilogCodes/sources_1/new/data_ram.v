`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/05/02 12:12:04
// Design Name:
// Module Name: data_ram
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module data_ram(
    input[31:0] addr,datain,
    input clk,
    input en,we,
    input bysel,
    input bycopy,
    input byexpand,
    output reg[31:0] dataout,
    output reg user_wr_en,
    output[31:0] user_wr_addr,
    output[31:0] user_wr_data
);
    localparam  DRAM_DATA_NUM = 32'd512;
    localparam  RAM_START = 32'H1001_0000;
    localparam  RAM_END = RAM_START + DRAM_DATA_NUM*4;

    localparam  DRAM_USER_INST_NUM = 32'd128;
    localparam  INST_RAM_START = 32'H0040_0000;
    localparam  INST_RAM_END = INST_RAM_START + DRAM_USER_INST_NUM*4;

    wire[31:0] real_addr;
    assign real_addr = addr - RAM_START;
    wire[8:0] word_addr;
    wire[1:0] byte_addr;
    assign word_addr = real_addr[10:2];
    assign byte_addr = real_addr[1:0];

    reg[31:0] bram_in;
    wire[31:0] bram_out;
    reg[3:0] bram_wea;



    always @ ( * ) begin
        if(en && addr >= RAM_START && addr < RAM_END)
            if(~bysel) begin
                dataout = bram_out;
            end
            else if(byexpand) begin
                case (byte_addr)
                    2'b00: dataout = {{24{bram_out[7]}},bram_out[7:0]};
                    2'b01: dataout = {{24{bram_out[15]}},bram_out[15:8]};
                    2'b10: dataout = {{24{bram_out[23]}},bram_out[23:16]};
                    2'b11: dataout = {{24{bram_out[31]}},bram_out[31:24]};
                    default: dataout = 32'bz;
                endcase
            end
            else begin
                dataout = 32'b0;
            end
        else
            dataout = 32'b0;
    end

    always @ ( * ) begin
        if(en && addr >= RAM_START && addr < RAM_END) begin
            if(~bysel) begin
                bram_in = datain;
                if(we)
                    bram_wea = 4'b1111;
                else
                    bram_wea = 4'b0000;
            end
            else if(bycopy) begin
                bram_in = {datain[7:0],datain[7:0],datain[7:0],datain[7:0]};
                case (byte_addr)
                    2'b00: bram_wea = 4'b0001;
                    2'b01: bram_wea = 4'b0010;
                    2'b10: bram_wea = 4'b0100;
                    2'b11: bram_wea = 4'b1000;
                    default: bram_wea = 4'b0000;
                endcase
            end
            else begin
                bram_in = 32'b0;
                bram_wea = 4'b0000;
            end
        end
        else begin
            bram_in = 32'b0;
            bram_wea = 4'b0000;
        end
    end

    assign user_wr_addr = addr - INST_RAM_START;
    assign user_wr_data = datain;
    always @ ( * ) begin
        if(~bysel && en && we && addr >= INST_RAM_START && addr < INST_RAM_END) begin
            user_wr_en = 1;
        end
        else begin
            user_wr_en = 0;
        end
    end

    // dram_data dram_data_u0(
    //     .a(word_addr),
    //     .d(dram_in),
    //     .clk(clk),
    //     .we(we),
    //     .spo(dram_out)
    // );

    bram_data bram_data_u0(
        .clka(clk),
        .ena(en),
        .wea(bram_wea),
        .addra(word_addr),
        .dina(bram_in),
        .douta(bram_out)
    );

endmodule
