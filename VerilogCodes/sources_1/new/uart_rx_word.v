`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/06/08 15:33:54
// Design Name:
// Module Name: uart_rx_word
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module uart_rx_word(
    input                        clk,              //clock input
	input                        rst_n,            //asynchronous reset input, low active
	output reg[31:0]              rx_data,          //received serial data
	output reg                   rx_data_valid,    //received serial data is valid
	input                        rx_pin            //serial data input
    );

    wire[7:0] rx_byte;
    wire rx_byte_valid;
    reg[31:0] rx_data_reg;

    reg[2:0] rx_state;
    localparam  S_RX_BYTE0 = 0;
    localparam  S_RX_BYTE1 = 1;
    localparam  S_RX_BYTE2 = 2;
    localparam  S_RX_BYTE3 = 3;
    localparam  S_RX_VALID = 4;


    always@(posedge clk or negedge rst_n) begin
        if(~rst_n) begin
            rx_data <= 32'b0;
            rx_data_reg <= 32'b0;
            rx_data_valid <= 1'b0;
            rx_state <= S_RX_BYTE0;
        end
        else if(rx_state <= S_RX_BYTE0) begin
            rx_data_valid <= 1'b0;
            if(rx_byte_valid) begin
                // rx_data_reg[31:24] <= rx_byte;
                rx_data_reg[7:0] <= rx_byte;
                rx_state <= S_RX_BYTE1;
            end
        end
        else if(rx_state <= S_RX_BYTE1) begin
            if(rx_byte_valid) begin
                // rx_data_reg[23:16] <= rx_byte;
                rx_data_reg[15:8] <= rx_byte;
                rx_state <= S_RX_BYTE2;
            end
        end
        else if(rx_state <= S_RX_BYTE2) begin
            if(rx_byte_valid) begin
                // rx_data_reg[15:8] <= rx_byte;
                rx_data_reg[23:16] <= rx_byte;
                rx_state <= S_RX_BYTE3;
            end
        end
        else if(rx_state <= S_RX_BYTE3) begin
            if(rx_byte_valid) begin
                // rx_data_reg[7:0] <= rx_byte;
                rx_data_reg[31:24] <= rx_byte;
                rx_state <= S_RX_VALID;
            end
        end
        else if(rx_state <= S_RX_VALID) begin
            rx_data <= rx_data_reg;
            rx_data_valid <= 1'b1;
            rx_state <= S_RX_BYTE0;
        end
    end


    uart_rx #(.CLK_FRE(10)) uart_rx_u1(
        .clk(clk),
        .rst_n(rst_n),
        .rx_data(rx_byte),
        .rx_data_valid(rx_byte_valid),
        .rx_data_ready(1'b1),
        .rx_pin(rx_pin)
    );
endmodule
