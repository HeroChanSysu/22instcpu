`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/05/02 12:54:58
// Design Name:
// Module Name: top
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module top(
    input rst_n,
    input sys_clk
);

    // wire[31:0] inst_address,instcode;
    // wire[31:0] data_address,data_save,data_load;
    // wire bysel,bycopy,byexpand;
    // wire dmwe,dmen;
    //
    // cpu_u cpu_u0(~rst_n,sys_clk,instcode,data_load,dmen,dmwe,data_save,inst_address,data_address,bysel,bycopy,byexpand);
    // inst_rom inst_rom_u0(sys_clk,inst_address,instcode);
    // data_ram data_ram_u0(data_address,data_save,sys_clk,dmen,dmwe,bysel,bycopy,byexpand,data_load);

endmodule
