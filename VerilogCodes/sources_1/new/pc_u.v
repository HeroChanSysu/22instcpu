`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/04/28 11:01:15
// Design Name:
// Module Name: pc_u
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module pc_u(
    input[31:0] dinrs,
    input[15:0] dinoffset,
    input[25:0] dintarget,
    input[1:0] pcsrc,
    input clock,
    input reset,
    output[31:0] pcout
    );

    reg[31:0] pc;
    reg[31:0] pc_next;

    always @ ( * ) begin
        case(pcsrc)
            2'b00: pc_next = pc + 32'd4;
            2'b01: pc_next = pc + 32'd4 + {{14{dinoffset[15]}},dinoffset,2'b0};
            2'b10: pc_next = dinrs;
            2'b11: pc_next = {pc[31:28],dintarget,2'b00};
        endcase
    end

    always @ ( posedge clock or posedge reset ) begin
        if(reset) begin
            // pc <= 32'H0040_0000;
            pc <= 32'H8FC0_0000;
        end
        else begin
            pc <= pc_next;
        end
    end
    assign pcout = pc;

endmodule
