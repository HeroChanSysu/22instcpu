`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/05/01 22:31:44
// Design Name:
// Module Name: inst_rom
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module inst_rom(
    input clk,
    input[31:0] addr,
    output[31:0] data,
    input user_wr_en,
    input[31:0] user_wr_addr,
    input[31:0] user_wr_data
    );

    localparam  DROM_INST_NUM_STARTUP = 32'd1024;
    localparam  PC_START_STARTUP = 32'H8FC0_0000;
    localparam  PC_END_STARTUP = PC_START_STARTUP + DROM_INST_NUM_STARTUP*4;

    localparam  DROM_INST_NUM_USER = 32'd1024;
    // localparam  PC_START_USER = 32'H0040_0024;
    localparam  PC_START_USER = 32'H0040_0000;
    localparam  PC_END_USER = PC_START_USER + DROM_INST_NUM_USER*4;

    wire[31:0] real_addr_startup;
    wire[31:0] real_addr_user;
    assign real_addr_startup = addr - PC_START_STARTUP;
    assign real_addr_user = addr - PC_START_USER;

    wire[31:0] bram_out_startup,bram_out_user;
    reg[31:0] data;

    always @ ( * ) begin
        if(addr >= PC_START_STARTUP && addr < PC_END_STARTUP )
            data = bram_out_startup;
        else if(addr >= PC_START_USER && addr < PC_END_USER)
            data = bram_out_user;
        else
            data = 32'b0;
    end


    bram_inst_startup bram_inst_startup(
        .clka(clk),
        .addra(real_addr_startup[31:2]),
        .douta(bram_out_startup)
    );

    bram_inst_user bram_inst_user(
        .clka(clk),
        .wea(user_wr_en),
        .addra(user_wr_addr[31:2]),
        .dina(user_wr_data),
        .clkb(clk),
        .addrb(real_addr_user[31:2]),
        .doutb(bram_out_user)
    );

endmodule
