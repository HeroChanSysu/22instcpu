`timescale 1ns / 100ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/06/15 19:42:51
// Design Name: 
// Module Name: SegEncode
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SegEncode(
    input[3:0] number,
    input dot,
    input on,
    output[7:0] seg_code
    );
    reg[7:0] out;
    assign seg_code=on ? (dot?out & 8'b0111_1111 : out) : 8'b1111_1111;
    
    parameter _0 = 8'b1100_0000, _1 = 8'b1111_1001, _2 = 8'b1010_0100, 
               _3 = 8'b1011_0000, _4 = 8'b1001_1001, _5 = 8'b1001_0010, 
               _6 = 8'b1000_0010, _7 = 8'b1111_1000, _8 = 8'b1000_0000,
               _9 = 8'b1001_0000, _A = 8'b1000_1000, _B = 8'b1000_0011,
               _C = 8'b1100_0110, _D = 8'b1010_0001, _E = 8'b1000_0110,
               _F = 8'b1000_1110;
   
    
    always@(*) begin
        case( number )
           4'd0 : out = _0;
           4'd1 : out = _1;
           4'd2 : out = _2;
           4'd3 : out = _3;
           4'd4 : out = _4;
           4'd5 : out = _5;
           4'd6 : out = _6;
           4'd7 : out = _7;
           4'd8 : out = _8;
           4'd9 : out = _9;
           4'd10: out = _A;
           4'd11: out = _B;
           4'd12: out = _C;
           4'd13: out = _D;       
           4'd14: out = _E;
           4'd15: out = _F;
           default:out= 8'b1111_1111;          
        endcase
        
    end
    
endmodule
