`timescale 1ns / 100ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/06/15 18:51:08
// Design Name: 
// Module Name: counter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module counter 
    #(parameter BIT_WIDTH=16,
      parameter MAX_CNT=49999)
    (
    input rstn,
    input clk,
    output overflow
    );
    reg [BIT_WIDTH-1:0] cnt;
    
    always@(negedge rstn or posedge clk) begin
        if(!rstn) begin
            cnt<=0;
        end
        else if(cnt==MAX_CNT) begin
            cnt<=0;
        end
        else begin
            cnt<=cnt+1'b1;
        end
    end
    
    assign overflow= cnt==MAX_CNT;
    
endmodule

