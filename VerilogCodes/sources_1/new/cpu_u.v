`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/05/02 12:39:47
// Design Name:
// Module Name: cpu_u
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module cpu_u(
    input rst_n,
    input sys_clk,
    input[3:0] key_pins,
    output[3:0] led_pins,
    output[5:0] seg_select_pins,
    output[7:0] seg_code_pins,
    output tx_pin,
    input rx_pin
);
    wire rst;
    assign rst = ~rst_n;
    wire[31:0] dinrs,pcout,aluD,memD,aluA,aluB,reg2mem; //reg heap in&out
    wire[31:0] dinA,dinB,dout; // alu in&out
    wire[15:0] dinoffset;
    wire[25:0] dintarget;
    wire[3:0] aluctr;
    wire[1:0] pcsrc;
    wire rfwe,dmen,dmwe,fbzero;
    wire[1:0] asel,dsel,alusel;
    wire bysel,bycopy,byexpand;
    wire fbgtz;

    wire[31:0] instcode,data_load,data_address,data_save;
    wire[31:0] data_load_ram,data_load_perip;
    assign data_load = data_load_ram | data_load_perip;

    wire[4:0] clks;
    wire tx_pin;

    wire user_wr_en;
    wire[31:0] user_wr_addr;
    wire[31:0] user_wr_data;

    pc_u pc_u0(dinrs,dinoffset,dintarget,pcsrc,clks[4],rst,pcout);
    rf_u rf_u0(instcode,aluD,memD,rfwe,asel,dsel,alusel,pcout,rst,clks[4],aluA,aluB,reg2mem);
    alu alu_u0(dinA,dinB,aluctr,dout,fbzero,fbgtz);
    ctrl_u ctrl_u0(instcode,fbzero,fbgtz,pcsrc,aluctr,rfwe,asel,dsel,alusel,dmen,dmwe,bysel,bycopy,byexpand);

    inst_rom inst_rom_u0(clks[0],pcout,instcode,user_wr_en,user_wr_addr,user_wr_data);
    data_ram data_ram_u0(data_address,data_save,clks[3],dmen,dmwe,bysel,bycopy,byexpand,data_load_ram,user_wr_en,user_wr_addr,user_wr_data);
    peripherals peripherals_u0(data_address,data_save,clks[3],rst,dmen,dmwe,data_load_perip,key_pins,led_pins,seg_select_pins,seg_code_pins,rx_pin,tx_pin);


    clock_div clk_div_u0(rst,sys_clk,clks);

    assign dinrs = aluA;
    assign dinoffset = instcode[15:0];
    assign data_address = dout;
    assign aluD = dout;
    assign memD = data_load;
    assign dinA = aluA;
    assign dinB = aluB;
    assign data_save = reg2mem;

    assign dintarget = instcode[25:0];

    // ila_pc ila_pc(
    //     .clk(sys_clk),
    //     .probe0(pcout),
    //     .probe1(instcode),
    //     .probe2(peripherals_u0.keys),
    //     .probe3(rf_u0.register[8]),
    //     .probe4(rf_u0.register[9]),
    //     .probe5(rf_u0.register[10]),
    //     .probe6(rf_u0.register[11])
    // );

    ila_pc ila_pc(
        sys_clk,pcout,instcode,peripherals_u0.keys,
        rf_u0.register[8],rf_u0.register[9],rf_u0.register[10],rf_u0.register[11],
        rf_u0.register[12],rf_u0.register[13],rf_u0.register[14],rf_u0.register[15],
        rf_u0.register[4],rf_u0.register[5],rf_u0.register[6],rf_u0.register[7],
        rf_u0.register[16],rf_u0.register[17],rf_u0.register[18],rf_u0.register[19],
        rf_u0.register[2],rf_u0.register[3]
    );

endmodule
