`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/04/28 11:11:39
// Design Name:
// Module Name: rf_u
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module rf_u(
    input[31:0] instW, // command
    input[31:0] aluD,memD, // data from alu and memory
    input rfwe, // reg wr en
    input[1:0] asel, // select rt or rd to wr, 0->rd; 1->rt; 2->$ra
    input[1:0] dsel, // select what to write, 0->data from alu; 1->data from mem; 2->pc+4
    input[1:0] alusel,
    // alusel[1] select what to pass to the alu_Ain 0->data of rs;1->data from sa
    // alusel[0] select what to pass to the alu_Bin 0->data of rt;1->data from immediate
    input[31:0] pc,
    input reset,
    input clock,
    output[31:0] aluA,aluB,reg2mem);

    reg[31:0] register[31:0]; // register heap

    wire[4:0] R1A,R2A;
    wire[31:0] R1D,R2D;
    reg[4:0] W1A;
    wire[31:0] Constant;
    wire[5:0] func;
    wire[4:0] sa;
    reg[31:0] W1D;
    reg[31:0] aluBin;
    reg[31:0] aluAin;

    integer index;

    localparam  RAM_START = 32'H1001_0000;
    always @ ( posedge clock or posedge reset) begin
        if(reset) begin
            for(index=0;index<29;index=index+1) begin
                register[index] <= 32'b0;
            end
            register[29] <= RAM_START+32'd511*4; // $sp
            register[30] <= 32'd0; // $fp
            register[31] <= 32'd0; // $ra
        end
        else begin
            if(rfwe && (W1A!=0))
                register[W1A] <= W1D;
        end
    end

    assign R1A = instW[25:21]; //rs
    assign R1D = register[R1A];

    assign R2A = instW[20:16]; //rt
    assign R2D = register[R2A];
    assign reg2mem = R2D; // sw instruction: save rt to the offset(immediate) of rs

    always @ ( * ) begin
        case(asel) // select rt or rd to wr, 0->rd; 1->rt
            2'b00:W1A = instW[15:11];
            2'b01:W1A = instW[20:16];
            2'b10:W1A = 5'd31; // $ra
            default:W1A = instW[15:11];
        endcase
    end

    always @ ( * ) begin
        case(dsel) // seleect what to write, 0->data from alu; 1->data from mem
            2'b00:W1D = aluD;
            2'b01:W1D = memD;
            2'b10:W1D = pc + 32'd4;
            default:W1D = aluD;
        endcase
    end

    assign Constant = {{16{instW[15]}},instW[15:0]}; // extend the immediate
    assign func = instW[5:0];
    assign sa = instW[10:6];

    always @ ( * ) begin
        case(alusel[1])// select what to pass to the alu_Ain 0->data of rs;1->data from sa
            1'b0: aluAin = R1D;
            1'b1: aluAin = sa;
            default: aluBin = R1D;
        endcase
    end
    always @ ( * ) begin
        case(alusel[0])// select what to pass to the alu_Bin 0->data of rt;1->data from immediate
            1'b0: aluBin = R2D;
            1'b1: aluBin = Constant;
            default: aluBin = R2D;
        endcase
    end

    assign aluB = aluBin;
    assign aluA = aluAin;

endmodule
