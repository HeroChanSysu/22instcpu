`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2022/05/01 19:08:33
// Design Name:
// Module Name: ctrl_u
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module ctrl_u(
    input[31:0] instW,
    input fbzero,
    input fbgtz,
    output reg[1:0] pcsrc,
    output reg[3:0] aluctr,
    output rfwe,
    output[1:0] asel,dsel,alusel,
    output dmen,dmwe,
    output bysel,
    output bycopy,
    output byexpand
);

    wire[5:0] opcode,fcode;
    wire rtype;
    wire inst_addu,inst_addiu,inst_ori,inst_xor;
    wire inst_lui,inst_bne,inst_jr,inst_lw,inst_sw;

    wire inst_mul,inst_and,inst_andi,inst_or,inst_xori;
    wire inst_sll,inst_srl,inst_beq,inst_bgtz,inst_j,inst_jal;
    wire inst_lb,inst_sb;

    assign opcode = instW[31:26];
    assign fcode = instW[5:0];
    assign rtype = ~|opcode;  // opcode == 6'b000000 => R type instruction

    wire[6:0] judgecode;
    assign judgecode = {rtype,fcode};

    assign inst_addu = (judgecode == 7'b1_100001);
    assign inst_addiu = (opcode == 6'b001001);
    assign inst_ori = (opcode == 6'b001101);
    assign inst_xor = (judgecode == 7'b1_100110);
    assign inst_lui = (opcode == 6'b001111);
    assign inst_bne = (opcode == 6'b000101);
    assign inst_jr = (judgecode == 7'b1_001000);
    assign inst_lw = (opcode == 6'b100011);
    assign inst_sw = (opcode == 6'b101011);

    assign inst_mul = (opcode == 6'b011100 && fcode == 6'b000010);
    assign inst_and = (judgecode == 7'b1100100);
    assign inst_andi = (opcode == 6'b001100);
    assign inst_or = (judgecode == 7'b1100101);
    assign inst_xori = (opcode == 6'b001110);
    assign inst_sll = (judgecode == 7'b1000000);
    assign inst_srl = (judgecode == 7'b1000010);
    assign inst_beq = (opcode == 6'b000100);
    assign inst_bgtz = (opcode == 6'b000111);
    assign inst_j = (opcode == 6'b000010);
    assign inst_jal = (opcode == 6'b000011);
    assign inst_lb = (opcode == 6'b100000);
    assign inst_sb = (opcode == 6'b101000);


    assign asel[0] = inst_addiu | inst_ori | inst_lui | inst_lw | inst_andi | inst_xori | inst_lb;
    assign asel[1] = inst_jal;

    assign dsel[0] = inst_lw | inst_lb;
    assign dsel[1] = inst_jal;

    assign rfwe = inst_addu | inst_addiu | inst_ori | inst_xor | inst_lui | inst_lw
    | inst_mul | inst_and | inst_andi | inst_or | inst_xori | inst_sll | inst_srl | inst_jal | inst_lb;

    assign alusel[0] = inst_addiu | inst_ori | inst_lui | inst_lw | inst_sw
                    | inst_andi | inst_xori | inst_lb | inst_sb;

    assign alusel[1] = inst_sll | inst_srl;

    assign dmen = inst_sw | inst_lw | inst_sb | inst_lb;
    assign dmwe = inst_sw | inst_sb;
    // assign pcsrc[1] = inst_jr;
    // assign pcsrc[0] = inst_bne & ~ fbzero;
    always @ ( * ) begin
        if( (inst_bne & ~ fbzero) | (inst_beq & fbzero) | (inst_bgtz & fbgtz)) begin
            pcsrc = 2'b01;
        end
        else if(inst_jr) begin
            pcsrc = 2'b10;
        end
        else if(inst_j | inst_jal) begin
            pcsrc = 2'b11;
        end
        else begin
            pcsrc = 2'b00;
        end
    end

    // assign aluctr[2] = inst_lui | inst_bne;
    // assign aluctr[1] = inst_ori | inst_xor;
    // assign aluctr[0] = inst_addu | inst_addiu | inst_xor | inst_bne | inst_lw | inst_sw;

    always @ ( * ) begin
        if(inst_addu | inst_addiu | inst_lw | inst_sw | inst_lb | inst_sb)
            aluctr = 4'b0001;
        else if(inst_ori | inst_or)
            aluctr = 4'b0010;
        else if(inst_xor | inst_xori)
            aluctr = 4'b0011;
        else if(inst_lui)
            aluctr = 4'b0100;
        else if(inst_bne | inst_beq)
            aluctr = 4'b0101;
        else if(inst_mul)
            aluctr = 4'b0110;
        else if(inst_and | inst_andi)
            aluctr = 4'b0111;
        else if(inst_sll)
            aluctr = 4'b1000;
        else if(inst_srl)
            aluctr = 4'b1001;
        else if(inst_bgtz)
            aluctr = 4'b1010;
        else
            aluctr = 4'b0000;
    end

    assign bysel = (inst_lb | inst_sb);
    assign bycopy = inst_sb;
    assign byexpand = inst_lb;


endmodule
