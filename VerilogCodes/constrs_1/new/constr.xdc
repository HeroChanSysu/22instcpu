############## NET - IOSTANDARD ######################
set_property CFGBVS VCCO [current_design]
set_property CONFIG_VOLTAGE 3.3 [current_design]
set_property BITSTREAM.CONFIG.UNUSEDPIN PULLUP [current_design]
#############SPI Configurate Setting##################
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
set_property CONFIG_MODE SPIx4 [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 50 [current_design]
############## clock and reset define##################
create_clock -period 20.000 [get_ports sys_clk]
set_property IOSTANDARD LVCMOS33 [get_ports sys_clk]
set_property PACKAGE_PIN Y18 [get_ports sys_clk]

set_property IOSTANDARD LVCMOS33 [get_ports rst_n]
set_property PACKAGE_PIN F20 [get_ports rst_n]

#############LED Setting###############################
set_property PACKAGE_PIN F19 [get_ports {led_pins[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led_pins[0]}]

set_property PACKAGE_PIN E21 [get_ports {led_pins[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led_pins[1]}]

set_property PACKAGE_PIN D20 [get_ports {led_pins[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led_pins[2]}]

set_property PACKAGE_PIN C20 [get_ports {led_pins[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led_pins[3]}]

############## key define##############################
set_property PACKAGE_PIN M13 [get_ports {key_pins[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {key_pins[0]}]

set_property PACKAGE_PIN K14 [get_ports {key_pins[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {key_pins[1]}]

set_property PACKAGE_PIN K13 [get_ports {key_pins[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {key_pins[2]}]

set_property PACKAGE_PIN L13 [get_ports {key_pins[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {key_pins[3]}]

#######################digital tube setting#############
set_property PACKAGE_PIN J5 [get_ports {seg_code_pins[0]}]
set_property PACKAGE_PIN M3 [get_ports {seg_code_pins[1]}]
set_property PACKAGE_PIN J6 [get_ports {seg_code_pins[2]}]
set_property PACKAGE_PIN H5 [get_ports {seg_code_pins[3]}]
set_property PACKAGE_PIN G4 [get_ports {seg_code_pins[4]}]
set_property PACKAGE_PIN K6  [get_ports {seg_code_pins[5]}]
set_property PACKAGE_PIN K3 [get_ports {seg_code_pins[6]}]
set_property PACKAGE_PIN H4 [get_ports {seg_code_pins[7]}]

set_property PACKAGE_PIN M2 [get_ports {seg_select_pins[0]}]
set_property PACKAGE_PIN N4 [get_ports {seg_select_pins[1]}]
set_property PACKAGE_PIN L5 [get_ports {seg_select_pins[2]}]
set_property PACKAGE_PIN L4 [get_ports {seg_select_pins[3]}]
set_property PACKAGE_PIN M16 [get_ports {seg_select_pins[4]}]
set_property PACKAGE_PIN M17 [get_ports {seg_select_pins[5]}]

set_property IOSTANDARD LVCMOS33 [get_ports {seg_code_pins[*]}]
set_property IOSTANDARD LVCMOS33 [get_ports {seg_select_pins[*]}]

############## usb uart define########################
set_property IOSTANDARD LVCMOS33 [get_ports rx_pin]
set_property PACKAGE_PIN G15 [get_ports rx_pin]

set_property IOSTANDARD LVCMOS33 [get_ports tx_pin]
set_property PACKAGE_PIN G16 [get_ports tx_pin]
