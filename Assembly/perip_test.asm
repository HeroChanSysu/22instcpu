# function:
# press any button then corredponding led on
# press key0 seg num + 1
# press key1 seg num + 0x10
# press key2 seg num + 0x1000
# press key3 seg num = 0

START:
lui $t0,0x1002 # base addr of perip
ori $t1,$zero,0 # seg display num
ori $t2,$zero,0
sw $t1,12($t0)  # set seg display = 000000
sw $t2,4($t0)  # set led = 0000
ori $t2,$zero,0x151f
sw $t2,8($t0)  # open seg display

lui $a0,0x006E # Bt
ori $a0,0x7442 #n
ori $a1,$zero,0x0A0D #\r\n
ori $a2,$zero,0
ori $a3,$zero,0

DETECT_LOOP:
lw $t2,0($t0) # read btn
sw $t2,4($t0) # set led = btn
andi $t3,$t2,0x0001
bgtz $t3,DETECT_KEY0
srl $t3,$t2,1
andi $t3,$t3,0x0001
bgtz $t3,DETECT_KEY1
srl $t3,$t2,2
andi $t3,$t3,0x0001
bgtz $t3,DETECT_KEY2
srl $t3,$t2,3
andi $t3,$t3,0x0001
bgtz $t3,DETECT_KEY3
j DETECT_LOOP # no btn detected, continue loop

DETECT_KEY0:
addiu $t1,$t1,0x0001
sw $t1,12($t0)
lui $a0,0x306E # Bt
ori $a0,0x7442 #n0
JAL TX_SEND
WAIT_RELEASE0:
lw $t2,0($t0) # read btn
andi $t3,$t2,0x0001
beq $t3,$zero,DETECT_LOOP
j WAIT_RELEASE0

DETECT_KEY1:
addiu $t1,$t1,0x0010
sw $t1,12($t0)
lui $a0,0x316E # Bt
ori $a0,0x7442 #n1
JAL TX_SEND
WAIT_RELEASE1:
lw $t2,0($t0) # read btn
srl $t3,$t2,1
andi $t3,$t3,0x0001
beq $t3,$zero,DETECT_LOOP
j WAIT_RELEASE1

DETECT_KEY2:
addiu $t1,$t1,0x1000
sw $t1,12($t0)
lui $a0,0x326E # Bt
ori $a0,0x7442 #n2
JAL TX_SEND
WAIT_RELEASE2:
lw $t2,0($t0) # read btn
srl $t3,$t2,2
andi $t3,$t3,0x0001
beq $t3,$zero,DETECT_LOOP
j WAIT_RELEASE2

DETECT_KEY3:
ori $t1,$zero,0
sw $t1,12($t0)
lui $a0,0x336E # Bt
ori $a0,0x7442 #n3
JAL TX_SEND
WAIT_RELEASE3:
lw $t2,0($t0) # read btn
srl $t3,$t2,3
andi $t3,$t3,0x0001
beq $t3,$zero,DETECT_LOOP
beq $t3,$zero,DETECT_LOOP
j WAIT_RELEASE3


TX_SEND:
addiu $sp,$sp,-4
sw $ra,0($sp)

ori $t5,$zero,1
sw $a0,20($t0)
sw $t5,16($t0)
JAL WAIT_TX
sw $a1,20($t0)
sw $t5,16($t0)
JAL WAIT_TX
sw $a2,20($t0)
sw $t5,16($t0)
JAL WAIT_TX
sw $a3,20($t0)
sw $t5,16($t0)
JAL WAIT_TX

lw $ra,0($sp)
addiu $sp,$sp,4
JR $ra


WAIT_TX:
WAIT_TX_LOOP:
lw $t7,16($t0)
bne $t7,$zero,WAIT_TX_LOOP
JR $ra

