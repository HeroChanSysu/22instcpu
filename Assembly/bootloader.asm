INIT:
lui $t0,0x1002
lui $t1,0x0040
DETECTLOOP:
lw $t2,0($t0) # read btn0
bne $t2,$zero,JUMP2PROGRAM # stop
lw $t2,24($t0)
beq $t2,$zero,DETECTLOOP
# rx get
sw $zero,24($t0)
lw $t3,28($t0)
sw $t3,0($t1)
addiu $t1,$t1,4
beq $zero,$zero,DETECTLOOP

JUMP2PROGRAM:
WAIT_RELEASE:
lw $t2,0($t0) # read btn
beq $t2,$zero,BTN_RELEASED
beq $zero,$zero,WAIT_RELEASE
BTN_RELEASED:
lui $ra,0x40
jr $ra
