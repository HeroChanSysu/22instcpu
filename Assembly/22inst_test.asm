START:
ori $a0,$zero,123
ori $a1,$zero,321
addiu $a2,$zero,-777

addu $s0,$zero,$a0
xor $s1,$a0,$a1
lui $s2,0x0040
BNELOOP1:
bne $zero,$zero,BNELOOP1
bne $a0,$zero,BNENEXT1
ori $a0,$zero,111
BNENEXT1:
addiu $s2,$s2,0x30
jr $s2
ori $a0,$zero,111
JRPOS:
lui $s2,0x1001
sw $a0,0($s2)
lw $s3,0($s2)

mul $t0,$a0,$a1
and $t1,$a0,$a1
andi $t2,$a0,0x000f
or $t3,$zero,0x1234
xori $t4,$a0,0xffff

sll $t5,$a1,2
srl $t6,$a1,2
BEQLOOP1:
beq $zero,$a0,BEQLOOP1
beq $zero,$zero,BEQNEXT1
ori $a0,$zero,111 
# this inst will never be excuted
BEQNEXT1:
BGTZLOOP1:
bgtz $zero,BGTZLOOP1
bgtz $a2,BGTZLOOP1
bgtz $a0,BGTZNEXT1
ori $a0,$zero,111 
# this inst will never be excuted
BGTZNEXT1:
j JNEXT
ori $a0,$zero,111 
# this inst will never be excuted
JNEXT:

ori $gp,$zero,0
lui $gp,0x1001
sw $a2,0($gp)
lw $t7($gp)
lb $t7,0($gp)
lb $t7,1($gp)
lb $t7,2($gp)
sb $zero,1($gp)
lw $t7,0($gp)


END:
nop
j END

